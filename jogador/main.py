from personagem import Personagem

jogador = Personagem('Andres', 'Guerreiro', 'Humano', {'arcana': 0})

print(f'--- Dados do jogador --- \
            \nNome: {jogador.name} \
            \nClasse: {jogador.classe} \
            \nRaca: {jogador.raca} \
            \nHabilidades: {jogador.skills}')
