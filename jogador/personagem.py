from dataclasses import dataclass


@dataclass
class Personagem:

    name: str = None
    classe: str = None
    raca: str = None
    skills: dict = None
